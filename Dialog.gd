extends Label


onready var tween = $Tween


var current_line := 0
var current_monoKey = "initial"


var monolog := {
	"initial" : ["string vide"],
	2 : [
		"qqch", "...", "...!!", "Thank you!",
		"My name is Blob",
		"Hey! Please!",
		"Could you please, by any chance, let your LMB go down onto the screen?"
	],
	3 : ["BLOUP", "BLIP"]
}


func _ready():
	update_text()


func _input(event):
	if event.is_action_pressed("click"):
		update_text()


func update_text():
	if current_line >= monolog.get(current_monoKey).size() or tween.is_active():
		tween.remove_all()
		percent_visible = 1.0
	else:
		text = monolog.get(current_monoKey)[current_line]
		tween.interpolate_property(self, "percent_visible", 0.0, 1.0, 1.0)
		tween.start()
		current_line += 1


func _on_Player_display_dialog(intuision: int):
	if monolog.has(intuision) :
		current_monoKey = intuision
		current_line = 0
		print("puf", intuision)
