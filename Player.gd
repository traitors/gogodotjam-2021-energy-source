extends Node2D

signal display_dialog(intuision)


var click_counter := 0

func _input(event):
	if event.is_action_pressed("click"):
		print(click_counter)
		click_counter += 1
		emit_signal("display_dialog", click_counter)


func _ready():
	pass # Replace with function body.
